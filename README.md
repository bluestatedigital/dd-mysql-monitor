MySQL metric augmentation for DataDog

Provides additional metrics that [dd-agent](https://github.com/DataDog/dd-agent)
does not.  Attempts to read existing DD MySQL agent config YAML file for
credentials, but those can be provided as command-line flags.

## building

    make

## running

    ./stage/dd-mysql-monitor \
        --debug \
        --mysql-server=localhost \
        --mysql-user=… \
        --mysql-pass=…


## running as a service

Using existing `/etc/dd-agent/conf.d/mysql.yaml`:

    $ make rpm
    $ rpm -Uvh stage/dd-mysql-monitor-<ver>-1.x86_64.rpm
    $ service dd-mysql-monitor start

