package agent_config

import (
    "os"
    "io/ioutil"
    
    "github.com/go-yaml/yaml"
)
type AgentConfig struct {
    Instances []struct{
        Server string `yaml:"server"`
        Port   int    `yaml:"port"`
        User   string `yaml:"user"`
        Pass   string `yaml:"pass"`
    } `yaml:"instances"`
}

func ReadFromFile(filename string) (cfg *AgentConfig, err error) {
    ifp, err := os.Open(filename)
    if err != nil {
        return
    }
    
    bytes, err := ioutil.ReadAll(ifp)
    if err != nil {
        return
    }
    
    cfg = &AgentConfig{}
    err = yaml.Unmarshal(bytes, cfg)
    
    return
}
