package agent_config_test

import (
    . "bitbucket.com/bluestatedigital/dd-mysql-monitor/agent_config"

    . "github.com/onsi/ginkgo"
    . "github.com/onsi/gomega"
    
    // "github.com/stretchr/testify/mock"
)

var _ = Describe("Agent Config", func() {
    It("reads from file", func() {
        agentConfig, err := ReadFromFile("config_test.yaml")
        
        Expect(err).To(BeNil())
        Expect(agentConfig).NotTo(BeNil())
        
        Expect(len(agentConfig.Instances)).To(Equal(1))
        Expect(agentConfig.Instances[0].Server).To(Equal("localhost"))
        Expect(agentConfig.Instances[0].Port).To(Equal(3306))
        Expect(agentConfig.Instances[0].User).To(Equal("someUser"))
        Expect(agentConfig.Instances[0].Pass).To(Equal("somePass"))
    })
})
