package main

import (
    "os"
    "os/signal"
    "syscall"
    "fmt"
    "strconv"
    "time"
    
    flags "github.com/jessevdk/go-flags"
    log "github.com/Sirupsen/logrus"
    
    "github.com/ooyala/go-dogstatsd"
    
    . "bitbucket.com/bluestatedigital/dd-mysql-monitor/mysql_status"
    "bitbucket.com/bluestatedigital/dd-mysql-monitor/agent_config"
)

var version string = "undef"

type Options struct {
    Debug bool     `env:"DEBUG"    long:"debug"    description:"enable debug"`
    LogFile string `env:"LOG_FILE" long:"log-file" description:"path to JSON log file"`
    
    AgentConfigPath string `env:"MYSQL_CONFIG" long:"mysql-config" default:"/etc/dd-agent/conf.d/mysql.yaml" description:"path to agent YAML config"`
    MySQLServer string     `env:"MYSQL_SERVER" long:"mysql-server"                                           description:"MySQL server"`
    MySQLPort   int        `env:"MYSQL_PORT"   long:"mysql-port"   default:"3306"                            description:"MySQL port"`
    MySQLUser   string     `env:"MYSQL_USER"   long:"mysql-user"                                             description:"MySQL username"`
    MySQLPass   string     `env:"MYSQL_PASS"   long:"mysql-pass"                                             description:"MySQL password"`
    
    UpdateInterval string `env:"UPDATE_INTERVAL" long:"interval" default:"10s" description:"how frequently to poll MySQL"`
    
    DogStatsdHost string `env:"STATS_HOST" long:"stats-host" default:"127.0.0.1" required:"true" description:"dogstatsd host"`
    DogStatsdPort int    `env:"STATS_PORT" long:"stats-port" default:"8125"      required:"true" description:"dogstatsd port"`
}

func main() {
    var opts Options

    _, err := flags.Parse(&opts)
    if err != nil {
        os.Exit(1)
    }
    
    if opts.Debug {
        log.SetLevel(log.DebugLevel)
    }
    
    if opts.LogFile != "" {
        logFp, err := os.OpenFile(opts.LogFile, os.O_WRONLY | os.O_APPEND | os.O_CREATE, 0600)
        checkError(fmt.Sprintf("error opening %s", opts.LogFile), err)
        
        defer logFp.Close()
        
        // ensure panic output goes to log file
        syscall.Dup2(int(logFp.Fd()), 1)
        syscall.Dup2(int(logFp.Fd()), 2)
        
        // log as JSON
        log.SetFormatter(&log.JSONFormatter{})
        
        // send output to file
        log.SetOutput(logFp)
    }

    // parse UpdateInterval
    updateInterval, err := time.ParseDuration(opts.UpdateInterval)
    checkError(fmt.Sprintf("invalid update interval %s", opts.UpdateInterval), err)
    
    // set up config
    mysqlServer := ""
    mysqlPort   := 3306
    mysqlUser   := ""
    mysqlPass   := ""
    
    agentConfig, err := agent_config.ReadFromFile(opts.AgentConfigPath)
    if err != nil {
        log.Errorf("unable to read agent config %s: %v; continuing", opts.AgentConfigPath, err)
    } else if len(agentConfig.Instances) > 0 {
        mysqlServer = agentConfig.Instances[0].Server
        mysqlPort   = agentConfig.Instances[0].Port
        mysqlUser   = agentConfig.Instances[0].User
        mysqlPass   = agentConfig.Instances[0].Pass
    }
    
    if mysqlServer == "" { mysqlServer = opts.MySQLServer }
    if mysqlPort   == 0  { mysqlPort   = opts.MySQLPort   }
    if mysqlUser   == "" { mysqlUser   = opts.MySQLUser   }
    if mysqlPass   == "" { mysqlPass   = opts.MySQLPass   }
    
    if (mysqlServer == "") || (mysqlUser == "") || (mysqlPass == "") {
        log.Fatal("invalid MySQL config")
    }

    log.Infof("version: %s", version)
    
    // Create the client
    dog, err := dogstatsd.New(fmt.Sprintf("%s:%d", opts.DogStatsdHost, opts.DogStatsdPort))
    checkError("unable to create dogstatsd client", err)
    defer dog.Close()

    statusChecker, err := NewMySQLStatus(mysqlServer, mysqlPort, mysqlUser, mysqlPass)
    checkError("error creating MySQLStatus", err)
    
    defer statusChecker.Close()
    
    // receive OS signals so we can cleanly shut down
    // use syscall signals because os only provides Interrupt and Kill
    signalChan := make(chan os.Signal)
    signal.Notify(signalChan, syscall.SIGINT, syscall.SIGTERM)

    log.Debug("starting main loop")

    go func() {
        metricMapping := map[string]string{
            "Aborted_clients":  "mysql.net.aborted_clients",
            "Aborted_connects": "mysql.net.aborted_connects",
            
            "Connection_errors_accept":          "mysql.net.conn_errors.accept",
            "Connection_errors_internal":        "mysql.net.conn_errors.internal",
            "Connection_errors_max_connections": "mysql.net.conn_errors.max_connections",
            "Connection_errors_peer_address":    "mysql.net.conn_errors.peer_address",
            "Connection_errors_select":          "mysql.net.conn_errors.select",
            "Connection_errors_tcpwrap":         "mysql.net.conn_errors.tcpwrap",
            
            "Select_full_join": "mysql.performance.select_full_join",
            "Select_range_check": "mysql.performance.select_range_check",
        }
        
        for {
            log.Debug("retrieving status")
            status, err := statusChecker.GetGlobalStatus()
            
            if err != nil {
                log.Errorf("unable to retrieve status: %v", err)
            } else {
                for statusKey, metricName := range metricMapping {
                    val, err := strconv.ParseFloat(status[statusKey], 64)
                    
                    if err != nil {
                        log.Errorf("unable to convert %s %s to float: %v", status[statusKey], val, err)
                    } else {
                        log.Debugf("%s: %f", statusKey, val)
                        
                        dog.Gauge(metricName, val, nil, 1)
                    }
                }
            }
            
            time.Sleep(updateInterval)
        }
    }()
    
    // Block until a signal is received
    log.Infof("fini: %v", <-signalChan)
}
