package mysql_status

import (
    "fmt"
    log "github.com/Sirupsen/logrus"


    "database/sql"
    _ "github.com/go-sql-driver/mysql"
)

type MySQLStatus struct {
    db *sql.DB
}

func NewMySQLStatus(server string, port int, user string, pass string) (*MySQLStatus, error) {
    db, err := sql.Open("mysql", fmt.Sprintf("%s:%s@tcp(%s:%d)/", user, pass, server, port))

    if err != nil {
        return nil, err
    } else {
        return &MySQLStatus{ db: db }, nil
    }
}

func (self *MySQLStatus) Close() {
    self.db.Close()
}

func (self *MySQLStatus) GetGlobalStatus() (status map[string]string, err error) {
    status = make(map[string]string)
    err = self.db.Ping()
    
    if err != nil {
        log.Debugf("unable to ping database: %v", err)
        return
    }
    
    rows, err := self.db.Query("show global status")
    if err != nil {
        log.Debugf("unable to query for status: %v", err)
        return
    }
    
    defer rows.Close()
    
    for rows.Next() {
        var name string
        var value string
        
        if err = rows.Scan(&name, &value); err != nil {
            log.Debugf("unable to retrieve data for row: %v", err)
            return
        }
        
        status[name] = value
    }
    
    err = rows.Err()
    if err != nil {
        log.Debugf("error iterating through rows: %v", err)
        return
    }
    
    return
}
