package mysql_status_test

import (
    . "github.com/onsi/ginkgo"
    . "github.com/onsi/gomega"

    "testing"
)

func TestMySQLStatus(t *testing.T) {
    RegisterFailHandler(Fail)
    RunSpecs(t, "MySQLStatus Suite")
}
